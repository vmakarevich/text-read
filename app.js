const app = require('express')()
const fs = require('fs')

app.get('/', (req, res) => {
    if(req.query.file && req.query.file === 'readthisfile') {
        const file = fs.readFileSync('./readthisfile', 'utf8')

        res.send(file)
    } else {
        res.send('File not existed')
    }
})
app.use('*', (req, res) => {
    res.status(404).send('Not found')
})

app.listen(9999)